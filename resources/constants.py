import datetime


class WebAppConstants:
    MAIN_PAGE_URL = "http://localhost:8000/"

    ADMIN_ID = 'admin'

    ADMIN_PASSWORD = "password"

    MAIN_PAGE_TITTLE_CONTENT = "Hello, world!"

    ADMIN_PAGE_TITTLE = "Site administration | Django site admin"


class TestCase1Constants:

    TEST_GROUP_TEXT = "TestGroup"

    NEW_USER_NAME = "TestName"

    NEW_USER_PASS = "TestPassword12345"


class TestGroup2Constants:

    select_by_text = 'Delete selected posts'


class CreateUser:
    MAIN_URL = "https://petstore.swagger.io/v2/user"
    NEW_USER_DATA_AS_JSON = {
        "id": 228322,
        "username": "mkazlou",
        "firstName": "mikhail",
        "lastName": "kazlou",
        "email": "example@email.com",
        "password": '12345',
        "phone": "+876543210",
        "userStatus": 700}


class Login:
    LOGIN_USER_DATA = {"username": "kms",
                       "password": "dadaya"}


class GetUserInfo:
    NAME = "mkazlou"


class DeleteUser:
    USER_NICKNAME = "mkazlou"
    RANDOM_USER_NICKNAME = 'random_nickname' + str(datetime.date.today())
    EXPECTED_CONTENT = '{"code":200,"type":"unknown","message":"mkazlou"}'


class AddNewPet:
    URL = "https://petstore.swagger.io/v2/pet"
    PET_DATA = {
        "id": 123,
        "category": {
            "id": 123,
            "name": "string"
        },
        "name": "doggie",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 123,
                "name": "snoopdoggiedog"
            }
        ],
        "status": "available"
    }


class EditPetName:
    NEW_PET_NAME = "Snoopy"
