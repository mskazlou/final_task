import psycopg2
from psycopg2 import Error
from psycopg2._psycopg import connection, cursor


# with conn:
#     with conn.cursor() as cursor:
#         cursor.execute(
#
#         column_names = [row[0] for row in cursor]
#
#     print("Column names: {}\n".format(column_names))

class DBMethods:

    @staticmethod
    def execute_script(connection: connection, script: str) -> cursor:
        cursor = connection.cursor()
        cursor.execute(script)
        return cursor


try:
    conn: connection = psycopg2.connect(dbname='postgres', user='postgres',
                                        password='postgres', host='localhost')
except Error as e:
    print("Connection error '{}' occured".format(e))

script1 = """SELECT column_name FROM information_schema.columns
              WHERE table_schema = 'public'
               AND
               table_name='TestGroup'"""

insert_group_script = """INSERT INTO auth_group(name) VALUES('TestGroup')"""

cursor = DBMethods.execute_script(conn, insert_group_script)

conn.commit()
cursor.close()
conn.close()

