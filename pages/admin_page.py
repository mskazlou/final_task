from pages.basepage import WebPage
from resources.locators import WebAppLocators, TestCase1Locators
from pages.elements import WebElements


class AdminPage(WebPage):

    def __init__(self, web_driver):
        super().__init__(web_driver=web_driver)

        self.go_to_admin_btn = WebElements(locator=WebAppLocators.GO_TO_ADMIN_BTN, webdriver=web_driver)

        self.username_field = WebElements(locator=WebAppLocators.USERNAME_FIELD, webdriver=web_driver)

        self.password_field = WebElements(locator=WebAppLocators.PASSWORD_FIELD, webdriver=web_driver)

        self.login_button = WebElements(locator=WebAppLocators.LOGIN_BUTTON, webdriver=web_driver)

        # TestCase1
        self.list_of_groups_btn = WebElements(locator=WebAppLocators.LIST_OF_GROUPS, webdriver=web_driver)

        self.test_group_element = WebElements(locator=TestCase1Locators.EXPECTED_TEST_GROUP, webdriver=web_driver)

        self.add_user_button = WebElements(locator=TestCase1Locators.ADD_USER_BUTTON, webdriver=web_driver)

        self.add_user_name_field = WebElements(locator=TestCase1Locators.ADD_USER_NAME_FIELD, webdriver=web_driver)

        self.add_user_password_field = WebElements(locator=TestCase1Locators.ADD_USER_PASSWORD_FIELD,
                                                   webdriver=web_driver)

        self.add_user_password_confirmation_field = WebElements(
            locator=TestCase1Locators.ADD_USER_PASSWORD_CONFIRMATION_FIELD,
            webdriver=web_driver)

        self.save_new_user_button = WebElements(locator=TestCase1Locators.SAVE_NEW_USER_BUTTON, webdriver=web_driver)

        self.success_message_element = WebElements(locator=TestCase1Locators.SUCCESS_MESSAGE_LOCATOR,
                                                   webdriver=web_driver)

        self.users_button = WebElements(locator=TestCase1Locators.USERS_BUTTON, webdriver=web_driver)

        self.test_users_button = WebElements(locator=TestCase1Locators.USERS_BUTTON, webdriver=web_driver)

        self.test_group_in_select_group = WebElements(locator=TestCase1Locators.TEST_GROUP_IN_SELECT_GROUP,
                                                      webdriver=web_driver)

        self.arrow_forward = WebElements(locator=TestCase1Locators.FORWARD_ARROW, webdriver=web_driver)

        self.test_group_in_user_page_element = WebElements(locator=TestCase1Locators.TEST_GROUP_IN_USER_PAGE_ELEMENT,
                                                           webdriver=web_driver)
