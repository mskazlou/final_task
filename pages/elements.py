from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import Select


class WebElements:
    _locator = None
    _web_driver = None
    _timeout = 10
    _element = None

    def __init__(self, locator=None, webdriver: WebDriver = None):

        self._web_driver = webdriver
        self._locator = locator

    def find(self):

        element = None
        try:
            element = WebDriverWait(self._web_driver, self._timeout).until(
                EC.presence_of_element_located(self._locator))

        except KeyboardInterrupt:
            print(f"Element with '{self._locator}' was not found on the page")
        return element

    def click(self):
        """ Wait and click the element. """

        element = self.find()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element(element). \
                click(on_element=element).perform()
        else:
            msg = 'Element with locator {0} not found'
            raise AttributeError(msg.format(self._locator))

    def send_keys(self, keys):

        element: WebElement = self.find()

        if element:
            element.click()
            element.clear()
            element.send_keys(keys)

        else:
            message = f"Element with '{self._locator}' not found."
            raise AttributeError(message)

    def get_text(self) -> str:

        element = self.find()
        text = ""

        try:
            text = str(element.text)

        except Exception as e:
            print('Error {0} has occured.'.format(e))
        return text

    def is_element_present(self):
        try:
            self.find()

        except NoSuchElementException:
            return False

        return True

    def select_by_text(self, text_value):
        element = self.find()

        try:
            select = Select(element)
            select.select_by_visible_text(text_value)

        except Exception as e:
            print("error '{0} was occurred'".format(e))

    def is_element_not_present(self):
        try:
            self.find()

        except NoSuchElementException:
            return True

        return False

    def get_attrib(self, attribute_name: str):
        """Return attribute value"""

        attribute_value = None
        element: WebElement = self.find()

        if element:
            attribute_value = element.get_attribute(attribute_name)
            return attribute_value

