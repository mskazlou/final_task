from pages.basepage import WebPage
from pages.elements import WebElements
from resources.locators import WebAppLocators, TestGroup2Locators, TestCase1Locators


class Group2Page(WebPage):

    def __init__(self, web_driver):
        super().__init__(web_driver=web_driver)

        # TestCase1Elements
        self.go_to_admin_btn = WebElements(locator=WebAppLocators.GO_TO_ADMIN_BTN, webdriver=web_driver)

        self.username_field = WebElements(locator=WebAppLocators.USERNAME_FIELD, webdriver=web_driver)

        self.password_field = WebElements(locator=WebAppLocators.PASSWORD_FIELD, webdriver=web_driver)

        self.login_button = WebElements(locator=WebAppLocators.LOGIN_BUTTON, webdriver=web_driver)

        # TestCase2Elements
        self.the_very_last_image = WebElements(locator=TestGroup2Locators.THE_VERY_LAST_IMAGE, webdriver=web_driver)

        self.posts_button = WebElements(locator=TestGroup2Locators.POSTS_BUTTON, webdriver=web_driver)

        self.the_very_last_admin_post_checkbox = WebElements(locator=TestGroup2Locators.THE_VERY_LAST_POST_CHECKBOX,
                                                             webdriver=web_driver)

        self.select_post_to_change_element = WebElements(locator=TestGroup2Locators.SELECT_POST_TO_CHANGE_ELEMENT,
                                                         webdriver=web_driver)

        self.go_button = WebElements(locator=TestGroup2Locators.GO_BUTTON, webdriver=web_driver)

        self.yes_im_sure_button = WebElements(locator=TestGroup2Locators.YES_IM_SURE_BUTTON, webdriver=web_driver)

        self.add_user_button = WebElements(locator=TestGroup2Locators.ADD_USER_BUTTON, webdriver=web_driver)

        self.add_user_name_field = WebElements(locator=TestCase1Locators.ADD_USER_NAME_FIELD, webdriver=web_driver)

        self.add_user_password_field = WebElements(locator=TestGroup2Locators.ADD_USER_PASSWORD_FIELD,
                                                   webdriver=web_driver)

        self.add_user_password_confirmation_field = WebElements(
            locator=TestGroup2Locators.ADD_USER_PASSWORD_CONFIRMATION_FIELD,
            webdriver=web_driver)
        self.save_new_user_button = WebElements(locator=TestCase1Locators.SAVE_NEW_USER_BUTTON, webdriver=web_driver)

        self.log_out_btn = WebElements(locator=TestGroup2Locators.LOG_OUT_BTN, webdriver=web_driver)

        self.add_staff_status_btn = WebElements(TestGroup2Locators.ADD_STAFF_STATUS_BTN, webdriver=web_driver)

        self.user_greetings_field = WebElements(TestGroup2Locators.GREETINGS_MESSAGE_FIELD,
                                                webdriver=web_driver)

