from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By


class WebPage:
    _web_driver = None

    def __init__(self, web_driver: WebDriver):
        self._web_driver = web_driver

    def get(self, url):
        self._web_driver.get(url)

    def get_title(self):
        return self._web_driver.title

    def refresh(self):
        self._web_driver.refresh()

    def click_staff_status_btn(self):
        self._web_driver.find_element(By.ID, "id_is_staff").click()
