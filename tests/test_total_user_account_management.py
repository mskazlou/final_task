import pytest
import requests
from baseAPI.baseapi import BaseMethods
from resources.constants import CreateUser
from baseAPI.assertions import Assertions
from resources.constants import Login
from resources.constants import GetUserInfo
import datetime
from resources.constants import DeleteUser


@pytest.mark.run(order=1)
class TestCreateUser(BaseMethods):
    """User creation testcase"""

    url = CreateUser.MAIN_URL
    new_user_data = CreateUser.NEW_USER_DATA_AS_JSON
    new_user_bad_data = {'almost nothing here': 'and even here'}

    def setup(self):

        self.response = requests.post(self.url, json=self.new_user_data)
        self.response_dict = self.get_json_value(self.response, 'message')

        self.bad_response = requests.post(self.url, json=self.new_user_bad_data)
        self.bad_response_dict = self.get_json_value(self.bad_response, 'message')

    def test_response_status_code(self):
        """Checks if response is ok."""
        assert self.response.status_code == 200, "Response status code is not equal to 200"

    def test_response_contents_user_id_as_a_key_message(self):
        """Checks if response has user id"""
        key_name = 'message'
        Assertions.assert_json_value_by_name(self.response_dict,
                                             key_name,
                                             expected_value=str(self.new_user_data['id']),
                                             error_message=f"Response '{key_name}' key value is not equal to user id")

    def test_bad_response_status_code(self):
        """Checks if response is ok."""

        assert self.bad_response.status_code == 200, "Response status code is not equal to 200 for some reasons."

    def test_bad_response_contents_user_id_as_a_key_message(self):
        """Checks if response has user id"""
        key_name = 'message'
        Assertions.assert_json_value_by_name(self.bad_response_dict,
                                             key_name,
                                             expected_value='0',
                                             error_message=f"Response '{key_name}' key value is not equal"
                                                           f" to expected id")


@pytest.mark.run(order=4)
class TestLogout(BaseMethods):
    url = CreateUser.MAIN_URL

    def setup(self):
        response = requests.get(self.url + "/logout")
        self.response_json = self.get_json_value(response, 'message')

    def test_response_status_code(self):
        """Checks if response is ok."""

        assert self.response_json['code'] == 200, "Response status code is not equal to 200"

    def test_response_message(self):
        """Checks if response message key contents 'ok' value"""

        assert self.response_json['message'] == 'ok', 'For some reasons response message is not ok'


@pytest.mark.run(order=2)
class TestLogin(BaseMethods):
    """Test user login procedure methods"""

    url = CreateUser.MAIN_URL + "/login"
    login_data = Login.LOGIN_USER_DATA

    def setup(self):
        self.ok_response = requests.get(url=self.url, params=self.login_data)
        self.ok_response_dict = self.get_json_value(self.ok_response, 'message')

    def test_response_status_code(self):
        """Checks if response is ok."""

        assert self.ok_response.status_code == 200, "Response status code is not equal to 200"

    def test_response_message(self):
        """Checks if response has 'logged in' message """

        key_value = self.get_json_value_by_key(self.ok_response_dict, 'message')
        assert 'logged in user' in key_value, "There is no needed message in the response"


@pytest.mark.run(order=3)
class TestGetUserInfo(BaseMethods):
    """Tests how we can get user info """

    user_nickname = GetUserInfo.NAME
    url = CreateUser.MAIN_URL + '/' + user_nickname
    expected_user_data = CreateUser.NEW_USER_DATA_AS_JSON

    def setup(self):
        self.response = requests.get(self.url)
        self.response_json = self.get_json_value(self.response, 'username')

        # Получаю рандомный никнейм (агент№ текущая дата)
        random_user_name = 'agent№{0}'.format(str(datetime.date.today()))
        self.bad_response = requests.get(self.url + random_user_name)
        bad_response_json = self.get_json_value(self.bad_response, 'message')

        # Получаю значение сообщения в JSON ответа
        self.bad_response_message_value = self.get_json_value_by_key(bad_response_json, 'message')

    def test_response_status_code(self):
        """Checks if response is ok."""

        assert self.response.status_code == 200, "Response status code is not equal to 200"

    def test_response_json_is_equal_to_expected_json(self):
        """Response contains the data of a previously created user.
        Checks if user data in response matches expected user data."""

        assert self.response_json == self.expected_user_data, "User data in response doesn't match expected user data."

    def test_bad_response_status_code(self):
        """Checks if bad_response status code is ok."""

        assert self.bad_response.status_code == 404, "For some reason bad response status code is not equal to 404"

    def test_bad_response_message_value(self):
        """Checks if response data contains error message."""

        assert self.bad_response_message_value == "User not found", "For some reason there us another message value"


@pytest.mark.run(order=5)
class TestDeleteUser(BaseMethods):
    """Delete user testcase"""

    user_nickname = DeleteUser.USER_NICKNAME
    url = CreateUser.MAIN_URL + '/'
    expected_content = DeleteUser.EXPECTED_CONTENT

    # contents 'random nickname' + current date in string format
    random_nickname = DeleteUser.RANDOM_USER_NICKNAME

    def setup(self):
        self.response = requests.delete(self.url + self.user_nickname)

        self.bad_request = requests.delete(self.url + self.random_nickname)

    def test_status_code(self):
        """Checks if response is ok."""

        assert self.response.status_code == 200, "Response status code is not equal to 200"

    def test_response_message(self):
        assert self.response.content.decode("utf-8") == self.expected_content, 'For some reasons response content is ' \
                                                                            'not equal to expected content'

    def test_bad_request_status_code(self):
        """Checks if response is not ok."""

        assert self.bad_request.status_code == 404, "Bad Response status code is not equal to 404"
