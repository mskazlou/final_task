import psycopg2
import pytest
from pages.group2_page import Group2Page
from resources.constants import WebAppConstants, TestGroup2Constants, TestCase1Constants


class TestGroup2:
    main_url = WebAppConstants.MAIN_PAGE_URL

    def test_case2(self, login_as_admin):
        """Checks if admin can delete the first image."""

        select_by_text = TestGroup2Constants.select_by_text

        page = Group2Page(web_driver=login_as_admin)

        page.get(self.main_url)

        page.the_very_last_image.is_element_present()
        last_image_src_value = page.the_very_last_image.get_attrib('src')

        page.go_to_admin_btn.click()
        page.posts_button.click()
        page.the_very_last_admin_post_checkbox.click()
        page.select_post_to_change_element.select_by_text(select_by_text)
        page.go_button.click()
        page.yes_im_sure_button.click()

        page.get(self.main_url)
        page.the_very_last_image.is_element_present()
        updated_last_image_src_value = page.the_very_last_image.get_attrib('src')

        assert not last_image_src_value == updated_last_image_src_value, "SRC attrib values shouldn't be equal!"

    @pytest.mark.usefixtures('delete_user_with_database')
    def test_case1(self, login_as_admin):
        """Checks if admin can create user with ability to login in web app"""
        new_user_name = TestCase1Constants.NEW_USER_NAME
        new_user_password = TestCase1Constants.NEW_USER_PASS

        page = Group2Page(web_driver=login_as_admin)
        page.get(self.main_url)
        page.go_to_admin_btn.click()
        page.add_user_button.click()
        page.add_user_name_field.send_keys(new_user_name)
        page.add_user_password_field.send_keys(new_user_password)
        page.add_user_password_confirmation_field.send_keys(new_user_password)
        page.save_new_user_button.click()
        page.click_staff_status_btn()
        page.save_new_user_button.click()

        with psycopg2.connect(dbname='postgres', user='postgres',
                              password='postgres', host='localhost') as connection:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                     SELECT username FROM auth_user
                     WHERE auth_user.username = '{new_user_name}'
                 """)
                db_new_user_name = cursor.fetchone()[0]
        assert new_user_name == db_new_user_name, f"User name '{db_new_user_name}' " \
                                                  f"should be equal to '{new_user_name}'."

        page.log_out_btn.click()
        page.refresh()
        page.add_user_name_field.send_keys(new_user_name)
        page.password_field.send_keys(new_user_password)
        page.login_button.click()
        greetings_message = page.user_greetings_field.get_text()
        assert new_user_name.upper() in greetings_message, "Greetings message should equal to new user name."
