import psycopg2
import pytest
from pages.admin_page import AdminPage
from resources.constants import WebAppConstants, TestCase1Constants


class TestLoginProcedure:
    """In this test_case we check if we have access to admin menu of the web app."""

    main_url = WebAppConstants.MAIN_PAGE_URL
    admin_id = WebAppConstants.ADMIN_ID

    def test_user_can_get_the_main_page(self, web_driver):
        """Checks if we can get to the main page of the app"""

        expected_title = WebAppConstants.MAIN_PAGE_TITTLE_CONTENT

        page = AdminPage(web_driver=web_driver)
        page.get(self.main_url)
        assert page.get_title() == expected_title, "Tittle content should be equal to expected."

    def test_user_has_access_to_admin_service(self, web_driver):
        """Checks if we have access to admin service."""

        admin_id = WebAppConstants.ADMIN_ID
        admin_password = WebAppConstants.ADMIN_PASSWORD
        expected_admin_page_title = WebAppConstants.ADMIN_PAGE_TITTLE

        page = AdminPage(web_driver=web_driver)
        page.go_to_admin_btn.click()
        page.username_field.send_keys(admin_id)
        page.password_field.send_keys(admin_password)
        page.login_button.click()

        assert page.get_title() == expected_admin_page_title, "Title content should be equal to expected."


@pytest.mark.usefixtures('delete_group_and_user_from_db')
class TestCase1:

    def test_user_can_see_test_group_in_the_list_of_groups(self, login_as_admin):
        """Checks if we can see recently created test group."""
        expected_test_group_text = TestCase1Constants.TEST_GROUP_TEXT

        page = AdminPage(web_driver=login_as_admin)
        page.list_of_groups_btn.click()

        assert page.test_group_element.get_text() == expected_test_group_text, "There is no TestGroup in the list" \
                                                                               " of groups."

    def test_admin_can_add_another_user(self, login_as_admin):
        """Checks if we can add new user to testing group"""
        expected_test_group_text = TestCase1Constants.TEST_GROUP_TEXT

        new_user_name = TestCase1Constants.NEW_USER_NAME
        new_user_password = TestCase1Constants.NEW_USER_PASS

        page = AdminPage(web_driver=login_as_admin)
        page.add_user_button.click()

        page.add_user_name_field.send_keys(new_user_name)
        page.add_user_password_field.send_keys(new_user_password)
        page.add_user_password_confirmation_field.send_keys(new_user_password)
        page.save_new_user_button.click()
        page.users_button.click()
        page.test_users_button.click()
        page.test_group_in_user_page_element.click()
        page.test_group_in_select_group.click()
        page.arrow_forward.click()
        page.save_new_user_button.click()

        with psycopg2.connect(dbname='postgres', user='postgres',
                              password='postgres', host='localhost') as connection:
            with connection.cursor() as cursor:
                cursor.execute(f"""
                    SELECT name FROM auth_group 
                    LEFT JOIN auth_user_groups on auth_group.id = auth_user_groups.group_id
                    LEFT JOIN auth_user on auth_user_groups.user_id = auth_user.id
                    WHERE username = '{new_user_name}'
                """)
                group_name = cursor.fetchone()[0]
        assert expected_test_group_text == group_name, f"{group_name} should be equal to" \
                                                       f" expected '{expected_test_group_text}' value."
