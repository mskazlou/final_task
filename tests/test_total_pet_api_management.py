from resources.constants import AddNewPet, EditPetName
import requests
from baseAPI.baseapi import BaseMethods


class TestAddPet(BaseMethods):
    URL = AddNewPet.URL
    PET_DATA = AddNewPet.PET_DATA

    def setup(self):
        self.response = requests.post(self.URL, json=self.PET_DATA)

        self.response_json_as_dict = self.get_json_value_for_pet(self.response)

    def test_response_status_code(self):
        """Checks if response status code is ok"""

        assert self.response.status_code == 200, "Response status code is not equal to 200."

    def test_response_content(self):
        """Checks if we have correct data in response JSON"""

        assert self.response_json_as_dict == self.PET_DATA, "Response JSON content doesn't match to Pet data"


class TestUserCanGetPetData(BaseMethods):

    URL = AddNewPet.URL + '/'
    PET_ID = AddNewPet.PET_DATA['id']

    def setup(self):
        self.response = requests.get(self.URL + f'{self.PET_ID}')
        self.response_json_as_dict = self.get_json_value_for_pet(self.response)

    def test_response_status_code(self):
        """Checks if response status code is ok"""

        assert self.response.status_code == 200, "Response status code is not equal to 200."

    def test_response_content(self):
        """Checks if we have correct data in response JSON"""

        assert self.response_json_as_dict == AddNewPet.PET_DATA


class TestUserCanEditPetName(BaseMethods):
    URL = AddNewPet.URL + '/'
    NEW_PET_NAME = EditPetName.NEW_PET_NAME
    EDITED_PET_DATA = AddNewPet.PET_DATA

    def setup(self):
        self.EDITED_PET_DATA['name'] = self.NEW_PET_NAME

        self.response = requests.put(self.URL, json=self.EDITED_PET_DATA)
        self.response_json_as_dict = self.get_json_value_for_pet(self.response)

    def test_status_code(self):
        """Checks if response status code is ok"""

        assert self.response.status_code == 200, "Response status code is not equal to 200."

    def test_response_contents_new_pet_name(self):
        """Checks if we have new pet name in response JSON"""

        assert self.response_json_as_dict['name'] == self.NEW_PET_NAME, 'Edited pet name should be equal to' \
                                                                        ' new pet name'
