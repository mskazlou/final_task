import time
import psycopg2
import pytest
from selenium import webdriver
from pathlib import Path
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options as chrome_options
from resources.constants import WebAppConstants, TestCase1Constants
from resources.locators import WebAppLocators
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import WebElement


def get_chrome_options():
    """Adds optional no gui mode"""
    options = chrome_options()
    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-extensions")
    return options


def get_chrome_service():
    chrome_driver_path = Path("/home/jrankel/Resources/chromedriver")
    service = Service(chrome_driver_path)
    return service


@pytest.fixture(scope="class")
def web_driver():
    service = get_chrome_service()

    # it's needed to add options to web_driver if we want to start tests in headless mode
    options = get_chrome_options()

    web_driver = webdriver.Chrome(service=service, options=options)
    web_driver.maximize_window()

    yield web_driver

    web_driver.quit()


@pytest.fixture()
def login_as_admin(timeout=5):

    # locators needed to log_in as admin
    username_field_locator = WebAppLocators.USERNAME_FIELD
    user_password_locator = WebAppLocators.PASSWORD_FIELD
    login_button_locator = WebAppLocators.LOGIN_BUTTON
    go_to_admin_button_locator = WebAppLocators.GO_TO_ADMIN_BTN

    # admin account data
    url = WebAppConstants.MAIN_PAGE_URL
    admin_id = WebAppConstants.ADMIN_ID
    admin_password = WebAppConstants.ADMIN_PASSWORD

    # webdriver_service
    service = get_chrome_service()

    # it's needed to add options to web_driver if we want to start tests in headless mode
    options = get_chrome_options()

    web_driver = webdriver.Chrome(service=service, options=options)
    web_driver.maximize_window()
    web_driver.get(url)

    # login as admin procedure
    go_to_admin_button: WebElement = WebDriverWait(web_driver, timeout).until(
        EC.presence_of_element_located(go_to_admin_button_locator)
    )
    go_to_admin_button.click()

    username_field_element: WebElement = WebDriverWait(web_driver, timeout).until(
        EC.presence_of_element_located(username_field_locator)
    )
    username_field_element.click()
    username_field_element.send_keys(admin_id)

    username_password_element: WebElement = WebDriverWait(web_driver, timeout).until(
        EC.presence_of_element_located(user_password_locator)
    )
    username_password_element.click()
    username_password_element.send_keys(admin_password)

    login_button: WebElement = WebDriverWait(web_driver, timeout).until(
        EC.presence_of_element_located(login_button_locator)
    )
    login_button.click()

    yield web_driver

    web_driver.quit()


@pytest.fixture(scope='function')
def delete_user_with_database():
    with psycopg2.connect(dbname='postgres', user='postgres',
                          password='postgres', host='localhost') as connection:
        with connection.cursor() as cur:
            yield
            cur.execute(f"""
            DELETE FROM auth_user WHERE username = '{TestCase1Constants.NEW_USER_NAME}'
            """)
            connection.commit()


@pytest.fixture(scope='class')
def delete_group_and_user_from_db():
    with psycopg2.connect(dbname='postgres', user='postgres',
                          password='postgres', host='localhost') as connection:
        with connection.cursor() as cursor:
            cursor.execute(f"""INSERT INTO auth_group (name) values ('{TestCase1Constants.TEST_GROUP_TEXT}') returning id""")
            user_group = cursor.fetchone()[0]
            connection.commit()
            yield
            cursor.execute(f"""DELETE FROM auth_user 
                               WHERE username = '{TestCase1Constants.NEW_USER_NAME}'""")

            cursor.execute(f"""DELETE FROM auth_user_groups 
                               WHERE group_id = {user_group}""")

            cursor.execute(f"""DELETE FROM auth_group 
                               WHERE name = '{TestCase1Constants.TEST_GROUP_TEXT}'""")
            connection.commit()
