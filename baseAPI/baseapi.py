from requests import Response
from json import JSONDecodeError


class BaseMethods:

    def get_json_value(self, response: Response, name: str) -> dict:
        try:
            response_as_dict = response.json()

        except JSONDecodeError:
            assert False, f"Response is not in JSON format. Response contents '{response.text}' "

        assert name in response_as_dict, f"There is no '{name}' key in response."
        return response_as_dict

    def get_json_value_by_key(self, json_dict: dict, key_name: str) -> str:
        """Returns value of json by key_name"""

        value = json_dict.get(key_name)
        if value is None:
            assert False, f"Response is not in Needed Format. Response text is {json_dict}"

        return value

    def get_json_value_for_pet(self, response: Response) -> dict:
        response_as_dict = None

        try:
            response_as_dict = response.json()

        except JSONDecodeError:
            assert False, f"Response is not in JSON format. Response contents '{response.text}' "

        return response_as_dict
