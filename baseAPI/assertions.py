class Assertions:

    @staticmethod
    def assert_json_value_by_name(json_as_dict, name, expected_value, error_message):

        assert json_as_dict[name] == expected_value, error_message
